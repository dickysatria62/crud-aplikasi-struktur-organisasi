<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[EmployeeController::class, 'index']);

Route::resource('/employee', App\http\Controllers\EmployeeController::class)->except('show');;
Route::resource('/company', App\http\Controllers\CompanyController::class)->except('show');;

// export pdf
Route::get('/exportpdf', [EmployeeController::class, 'exportpdf'])->name('exportpdf');

// export excel
Route::get('/exportExcel', [EmployeeController::class, 'exportExcel'])->name('exportExcel');
