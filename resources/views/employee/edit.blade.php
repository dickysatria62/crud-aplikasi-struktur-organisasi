<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD LARAVEL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
    <h2 class="text-center my-4">Edit Data Employee</h2>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card">
            <div class="card-body">
              <form action="{{ route('employee.update', $employee->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                    <label for="posisi" class="form-label">Posisi</label>
                    <input type="text" name="posisi" class="form-control" id="posisi" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                      <label for="company_id" class="form-tabel">Perusahaan</label>
                      <select class="form-select" name="company_id">
                          @foreach($companies as $company)
                          @if(old('company_id') == $company->id)
                             <option value="{{ $company->id }}" selected>{{ $company->company }}</option>
                             @else
                             <option value="{{ $company->id }}">{{ $company->company }}</option>
                             @endif
                          @endforeach
                      </select>
                    </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>
