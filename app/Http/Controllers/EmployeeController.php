<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\company;
use App\Models\employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
// use App\User;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('employees')
        ->join('companies','companies.id','=','employees.company_id')
        ->select("employees.*","companies.company")
        ->paginate(9);

        return view('employee.index', compact('data'));
    }

    public function create()
    {
        return view('employee.create',[
            'companies' => company::all()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'posisi' => 'required|string|max:255',
            'company_id' => 'required',
        ]);
        employee::create($request->all());
        return redirect()->route('employee.index')->with('success', 'data berhasil ditambahkan');

        // dd($request->all());
    }

    public function edit(employee $employee)
    {
        return view('employee.edit',[
            'employee' => $employee,
            'companies' => company::all(),
        ]);
    }

    public function update(Request $request, employee $employee)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'posisi' => 'required|string|max:255',
            'company_id' => 'required',
        ]);
        $employee->update($request->all());
        return redirect()->route('employee.index')->with('success', 'data berhasil diupdate');
    }

    public function destroy(employee $employee)
    {
        $employee->delete();
        return redirect()->route('employee.index')->with('success', 'data berhasil dihapus');
    }

    public function exportpdf()
    {
        $data = DB::table('employees')
        ->join('companies','companies.id','=','employees.company_id')
        ->select("employees.*","companies.company")->get();

        view()->share('data', $data);
        $pdf = PDF::loadview('employee.export-pdf');
        return $pdf->download('Employee.pdf');
    }

    public function exportExcel()
    {
        // $data = employee::all();

        return (new FastExcel(
            DB::table('employees')
            ->join('companies','companies.id','=','employees.company_id')
            ->select("employees.*","companies.company")->get()
            ))->download('Employee.xlsx', function ($data) {
            return [
                'id' => $data->id,
                'nama' => $data->nama,
                'Posisi' => $data->posisi,
                'Perusahaan' => $data->company,

            ];
        });
    }
}
