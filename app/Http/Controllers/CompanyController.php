<?php

namespace App\Http\Controllers;

// use App\Models\employee;

use App\Models\company;
use Illuminate\Http\Request;
use PDF;

class CompanyController extends Controller
{
    public function index()
    {
        return view('company.index', [
            'data' => company::all()
        ]);
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'company' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
        ]);

        company::create($request->all());
        return redirect()->route('company.index')->with('success', 'Company has been added!');

        // dd($request->all());
    }
    public function edit(company $company)
    {
        return view('company.edit', compact('company'));
    }

    public function update(Request $request, company $company)
    {
        $request->validate([
            'company' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
        ]);

        $company->update($request->all());

        return redirect()->route('company.index')->with('success', 'Company telah berhasil diupdate');
    }

    public function destroy(company $company)
    {
        $company->delete();
        return redirect()->route('company.index')->with('success', 'data berhasil dihapus');
    }

    // public function exportpdf()
    // {
    //     $data = company::all();
    //     view()->share('data', $data);
    //     $pdf = PDF::loadview('dataPegawai-pdf');
    //     return $pdf->download('data.pdf');
    // }
}
